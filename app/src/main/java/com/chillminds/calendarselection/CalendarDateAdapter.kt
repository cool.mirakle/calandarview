package com.chillminds.calendarselection

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.util.toRange
import androidx.recyclerview.widget.RecyclerView
import java.time.LocalDate
import kotlin.reflect.KFunction1

@RequiresApi(Build.VERSION_CODES.O)
class CalendarDateAdapter(
    var calendarInfo: CalendarInformation,
    var callback: KFunction1<LocalDate, Unit>
) : RecyclerView.Adapter<CalendarDateAdapter.CalendarDatesViewHolder>() {

    class CalendarDatesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val dateTextView: TextView = view.findViewById(R.id.dateTextView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CalendarDatesViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.calendar_date_view, parent, false)

        return CalendarDatesViewHolder(view)
    }

    override fun onBindViewHolder(holder: CalendarDatesViewHolder, position: Int) {
        val range = calendarInfo.calendarData.getDatesForTheMonth()
        val currentDate = range.start.plusDays(position.toLong())

        holder.dateTextView.setOnClickListener {
            callback(currentDate)
        }

        var isClickable = true

        if (currentDate.monthValue == calendarInfo.calendarData.getViewMonth().monthValue) {
            holder.dateTextView.text = "${currentDate.dayOfMonth}"
            if (currentDate.isInRange(-6)) {
                holder.dateTextView.isClickable = true
                holder.dateTextView.isEnabled = true
                isClickable = true
            } else {
                holder.dateTextView.isClickable = false
                holder.dateTextView.isEnabled = false
                isClickable = false
            }

        } else {
            holder.dateTextView.text = ""
            holder.dateTextView.setBackgroundColor(
                ContextCompat.getColor(
                    holder.dateTextView.context,
                    R.color.white
                )
            )
            isClickable = false
            holder.dateTextView.isClickable = false
            holder.dateTextView.isEnabled = false
        }

        if (currentDate.isItSame(calendarInfo.calendarData.getSelectedDate())) {
            holder.dateTextView.setBackgroundColor(
                ContextCompat.getColor(
                    holder.dateTextView.context,
                    if (isClickable) R.color.light_blue else R.color.white
                )
            )
            holder.dateTextView.setTextColor(
                ContextCompat.getColor(
                    holder.dateTextView.context,
                    R.color.red
                )
            )
        } else {
            holder.dateTextView.setBackgroundColor(
                ContextCompat.getColor(
                    holder.dateTextView.context,
                    R.color.white
                )
            )
            if (calendarInfo.isItToday(currentDate)) {

                holder.dateTextView.setTextColor(
                    ContextCompat.getColor(
                        holder.dateTextView.context,
                        R.color.teal_200
                    )
                )
            } else {
                holder.dateTextView.setTextColor(
                    ContextCompat.getColor(
                        holder.dateTextView.context,
                        if (isClickable) R.color.black else R.color.grey
                    )
                )
            }
        }

    }

    override fun getItemCount(): Int {
        return calendarInfo.calendarData.getDatesForTheMonth().toRange().getDaysCount()
    }

}
