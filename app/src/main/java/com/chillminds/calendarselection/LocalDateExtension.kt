package com.chillminds.calendarselection

import android.os.Build
import android.util.Range
import androidx.annotation.RequiresApi
import java.time.Duration
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
fun Range<LocalDate>.getDaysCount(): Int {
    return Duration.between(this.lower.atStartOfDay(), this.upper.atStartOfDay())
        .toDays().toInt()
}

@RequiresApi(Build.VERSION_CODES.O)
fun LocalDate.isItSame(selectedDate: LocalDate?): Boolean {
    return selectedDate != null && this.year == selectedDate.year && this.monthValue == selectedDate.monthValue && this.dayOfMonth == selectedDate.dayOfMonth
}

@RequiresApi(Build.VERSION_CODES.O)
fun LocalDate.isInRange(dateRangeCount: Int): Boolean {
    val start = LocalDate.now().toEpochDay()
    val end = LocalDate.now()
        .plusDays(dateRangeCount.toLong()).toEpochDay()

    return if (start > end) {
        this.toEpochDay() in end..start
    } else {
        this.toEpochDay() in start..end
    }

}