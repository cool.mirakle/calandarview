package com.chillminds.calendarselection

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButtonToggleGroup
import java.time.LocalDate
import java.time.MonthDay

@RequiresApi(Build.VERSION_CODES.O)
class MainActivity : AppCompatActivity() {

    private val calendarInfo by lazy {
        CalendarInformation()
    }

    private lateinit var datesCalendarView: RecyclerView
    private lateinit var buttonGroup: MaterialButtonToggleGroup
    lateinit var calendarAdapter: CalendarDateAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        datesCalendarView = findViewById(R.id.datesCalendarView)
        buttonGroup = findViewById(R.id.materialButtonToggleGroup)

        findViewById<Button>(R.id.previousMonthButton).setOnClickListener {
            updateCalendarView(-1)
        }
        findViewById<Button>(R.id.nextMonthButton).setOnClickListener {
            updateCalendarView(1)
        }

        setDateRecyclerView()
    }

    private fun updateCalendarView(numberOfMonth: Int) {
        calendarInfo.goBack(numberOfMonth)
        setMonthAndYear()
        calendarAdapter.notifyDataSetChanged()
    }

    private fun setMonthAndYear() {
        findViewById<TextView>(R.id.monthTextView).text = MonthDay.of(
            calendarInfo.calendarData.viewingMonth,
            1
        ).month.name
        findViewById<TextView>(R.id.yearTextView).text = "${calendarInfo.calendarData.viewingYear}"
    }

    private fun setDateRecyclerView() {
        calendarAdapter = CalendarDateAdapter(
            calendarInfo, ::onSelectDate
        )
        datesCalendarView.layoutManager =
            GridLayoutManager(this, 7)
        datesCalendarView.adapter = calendarAdapter
        setMonthAndYear()
    }

    private fun onSelectDate(localDate: LocalDate) {
        calendarInfo.calendarData.apply {
            selectedDay = localDate
        }
        calendarAdapter.notifyDataSetChanged()
    }
}