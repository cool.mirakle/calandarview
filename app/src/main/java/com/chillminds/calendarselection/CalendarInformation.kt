package com.chillminds.calendarselection

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
class CalendarInformation {

    val today = LocalDate.now()

    val calendarData = CalendarData(today.year, today.monthValue, null)

    fun goBack(numberOfMonths: Int) {
        val updatedMonth = calendarData.getViewMonth().plusMonths(numberOfMonths.toLong())
        calendarData.apply {
            viewingYear = updatedMonth.year
            viewingMonth = updatedMonth.monthValue
        }

    }

    fun isItToday(currentDate: LocalDate): Boolean {
        return today.year == currentDate.year && currentDate.monthValue == today.monthValue && today.dayOfMonth == currentDate.dayOfMonth
    }
}