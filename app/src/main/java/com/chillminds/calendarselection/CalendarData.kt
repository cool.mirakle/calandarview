package com.chillminds.calendarselection

import android.os.Build
import androidx.annotation.RequiresApi
import java.time.DayOfWeek
import java.time.LocalDate

@RequiresApi(Build.VERSION_CODES.O)
data class CalendarData(var viewingYear: Int, var viewingMonth: Int, var selectedDay: LocalDate?) {

    fun getDatesForTheMonth(): ClosedRange<LocalDate> {

        var startDate = LocalDate.of(viewingYear, viewingMonth, 1)

        startDate = when (startDate.dayOfWeek) {
            DayOfWeek.SUNDAY -> startDate.minusDays(0)
            DayOfWeek.MONDAY -> startDate.minusDays(1)
            DayOfWeek.TUESDAY -> startDate.minusDays(2)
            DayOfWeek.WEDNESDAY -> startDate.minusDays(3)
            DayOfWeek.THURSDAY -> startDate.minusDays(4)
            DayOfWeek.FRIDAY -> startDate.minusDays(5)
            DayOfWeek.SATURDAY -> startDate.minusDays(6)
        }

        val endDate = LocalDate.of(viewingYear, viewingMonth, 1).plusMonths(1).withDayOfMonth(1)

        return startDate..endDate
    }

    fun getSelectedDate(): LocalDate? {
        return selectedDay
    }
    fun getViewMonth(): LocalDate {
        return LocalDate.of(viewingYear, viewingMonth,1)
    }


}
